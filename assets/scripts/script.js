//array - can hold multiple values in a variable (square bracket);

//Objects - curly brace;
//name : "Brandon" >>key value pair, separated by comma;

let student = {
	name: "Brandon",
	age: 21,
	address : {
		stName:"Oxford St.",
		barangay: "Brgy. 121",
		city: "Makati City",
	},
	favoriteFood: ["Sinigang", "Tinola", "Fried Chicken"], //array inside an object;
	mother: {
		name: "Eloisa",
		age: 61,
		address : {
		stName:"Oxford St.",
		barangay: "Brgy. 121",
		city: "Makati City",
	}
}
}

//to access a data inside an object, we'll use the dot notation;

//student.favoriteFood.forEach(function(food);
 
student.favoriteFood.forEach(function(food){
	console.log(food);
})

student['name']
