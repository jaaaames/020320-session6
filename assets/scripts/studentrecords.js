let b55 = [];

// function to add students;

function enroll(name, gender, course){
	
	let student = {};
	student.name = name;
	student.gender = gender;
	student.course = course;
	student.maxAbsences = 5;

	b55.push(student);
	return student;

	// second way
	// let student = {
	// 	name: name,
	// 	gender: gender,
	// 	course: course
	// }

	//third way (shortcut)
	// let student = {
	// 	name,
	// 	gender,
	// 	course
	// }

}

function showStudents(){
	b55.forEach(function(student, index){
		console.log("Student Id: " + (index+1) + ", Student Name: " + student.name + ", Gender: " + student.gender + ", Course: " + student.course + ", Max Absences: " + student.maxAbsences);
	})
}

function markAsAbsent(studentNo){
	let realIndex = studentNo - 1;

    b55.forEach(function(student, index){         
    	if(index === realIndex){
			student.maxAbsences--
		}     
	}) 
}

